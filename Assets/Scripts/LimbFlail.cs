﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class LimbFlail : MonoBehaviour
{
	public Transform target;

	Vector3 rest;
	float timeOffset;

	void Start()
	{
		rest = target.localPosition;
		timeOffset = Random.Range(0f, 1f);
	}

	void Update()
	{
		Vector3 pos = target.localPosition;

		pos.y = rest.y + Mathf.Sin(Time.time * 10 + timeOffset) * 0.01f;

		target.localPosition = pos;
	}
}
