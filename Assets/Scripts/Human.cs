﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Human : MonoBehaviour
{
    public int var = 1;

    public float maxMoveSpeed = 1;
    public Vector2 dir = new Vector2(-1, 0);
    public Material zombieMaterial;

    public Collider2D zombieSensor;
    public float zombieProgress = 0;

    public AudioClip[] scaredClips;
    bool scared = false;

    ContactPoint2D[] contacts = new ContactPoint2D[8];

    new Rigidbody2D rigidbody;
    new Renderer renderer;
	AudioSource audioSource;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        renderer = GetComponent<Renderer>();
		audioSource = GetComponent<AudioSource>();
        audioSource.clip = scaredClips[Random.Range(0, scaredClips.Length - 1)];
    }

    void FixedUpdate()
    {
        // update contacts array
        contacts = new ContactPoint2D[8];
        rigidbody.GetContacts(contacts);

        if (!scared) {
            ContactFilter2D zombieFilter = new ContactFilter2D();
            zombieFilter.SetLayerMask(1 << 8);
            Collider2D[] zombies = new Collider2D[1];
            zombieSensor.OverlapCollider(zombieFilter, zombies);
            if (zombies[0])
            {
                dir.x = Mathf.Sign(transform.position.x - zombies[0].transform.position.x);
                audioSource.Play();
                scared = true;
            }
        }

        // only applying force if we're under max speed
        if (Vector2.Dot(dir, rigidbody.velocity) < maxMoveSpeed)
        {
            rigidbody.AddForce(dir * 10);
        }

        Vector3 scale = transform.localScale;
        if (Mathf.Sign(scale.x) - Mathf.Sign(dir.x) != 0)
        {
            scale.x *= -1;
            transform.localScale = scale;
        }

        // check if turned
        if (zombieProgress >= 1)
        {
            Turn();
        }
    }

    public void Turn()
    {
        Destroy(gameObject);
        GameManager.SpawnZombie(transform.position, var);
    }
}
