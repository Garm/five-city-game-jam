﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class IgnoreCollision : MonoBehaviour
{
	public Collider2D[] colliders;

	void Start()
	{
		Collider2D me = GetComponent<Collider2D>();
		foreach (Collider2D col in colliders)
		{
			Physics2D.IgnoreCollision(me, col);
		}
	}
}
