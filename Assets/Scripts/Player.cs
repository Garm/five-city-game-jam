﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Player : MonoBehaviour
{
	void Update()
	{
		if (Input.GetButtonDown("Fire1"))
		{
			Vector3 mouseWorldPos = Camera.main.ScreenToWorldPoint(Input.mousePosition);
			mouseWorldPos.z = 0;
			GameManager.SpawnMeat(mouseWorldPos);
		}

		Vector3 position = transform.position;
		position += new Vector3(Input.GetAxis("Horizontal"), Input.GetAxis("Vertical")) * Time.deltaTime * 10;
		position.y = Mathf.Max(position.y, 4);
		transform.position = position;
	}
}
