﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Background : MonoBehaviour
{
	public float parallaxStrength = 1f;

	Material material;

	void Start()
	{
		material = GetComponent<SpriteRenderer>().material;
	}

    void FixedUpdate()
    {
		Vector2 offset = material.mainTextureOffset;
		offset.x = -Camera.main.transform.position.x * parallaxStrength;
		material.mainTextureOffset = offset;
    }
}
