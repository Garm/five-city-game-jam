﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Door : MonoBehaviour
{
	public GameObject open;

	public float toughness = 1f;

	public float progress = 0f;
	public int dir = 1;

	bool broken = false;

	new Collider2D collider;
	new SpriteRenderer renderer;
	AudioSource audioSource;

	void Start()
	{
		collider = GetComponent<Collider2D>();
		renderer = GetComponent<SpriteRenderer>();
		audioSource = GetComponent<AudioSource>();
	}

	void Update()
	{
		if (!broken && progress >= toughness)
		{
			Destroy(collider);
			Destroy(renderer);
			open.SetActive(true);
			Vector3 scale = open.transform.localScale;
			scale.x = (float)dir;
			open.transform.localScale = scale;

			audioSource.Play();

			broken = true;
		}
	}
}
