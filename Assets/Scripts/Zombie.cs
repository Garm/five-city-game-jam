﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Zombie : MonoBehaviour
{
    public float maxMoveSpeed = 1;
    public float maxClimbSpeed = 1;
    public AudioClip[] growlClips;
    public Vector2 dir = new Vector2(-1, 0);

    bool justGotMounted = false;
    bool justGotCarried = false;

    ContactPoint2D[] contacts = new ContactPoint2D[8];

    new Rigidbody2D rigidbody;
    CircleCollider2D sensor;
    AudioSource audioSource;

    float moveSpeed;
    float climbSpeed;

    void Start()
    {
        rigidbody = GetComponent<Rigidbody2D>();
        sensor = GetComponent<CircleCollider2D>();
        audioSource = GetComponent<AudioSource>();

        moveSpeed = maxMoveSpeed;
        climbSpeed = maxClimbSpeed;

        StartCoroutine(Growl());
    }

    void FixedUpdate()
    {
        Vector3 scale = transform.localScale;
        if (Mathf.Sign(scale.x) - Mathf.Sign(dir.x) != 0)
        {
            scale.x *= -1;
            transform.localScale = scale;
        }

        // update contacts array
        contacts = new ContactPoint2D[8];
        rigidbody.GetContacts(contacts);

        // climbing logic
        if (justGotCarried)
        {
            bool hitHead = false;
            foreach (ContactPoint2D contact in contacts)
            {
                if (contact.normal.y < 0)
                {
                    hitHead = true;
                }
            }

            if (!hitHead && rigidbody.velocity.y < climbSpeed)
            {
                rigidbody.AddForce(new Vector2(0, 100));
            }

            justGotCarried = false;
        }
        else
        {
            // only applying force if we're under max speed
            if (Vector2.Dot(dir, rigidbody.velocity) < moveSpeed)
            {
                rigidbody.AddForce(dir * 100);
            }
        }
        justGotMounted = false;

        foreach (ContactPoint2D contact in contacts)
        {
            if (contact.collider && contact.collider.gameObject.layer == 11)
            {
                Door other = contact.collider.gameObject.GetComponent<Door>();
                other.progress += Time.deltaTime;
                other.dir = (int)Mathf.Sign(dir.x);
                break;
            }
        }

        // check if were contacting something opposite to move dir
        foreach (ContactPoint2D contact in contacts)
        {
            if (contact.collider && contact.collider.gameObject.layer == 10)
            {
                Meat other = contact.collider.gameObject.GetComponent<Meat>();
                other.eatenProgress += Time.deltaTime;
            }
            else if (contact.collider && contact.collider.gameObject.layer == 9)
            {
                Human other = contact.collider.gameObject.GetComponent<Human>();
                other.zombieProgress += Time.deltaTime;
            }
            else if (contact.collider && contact.collider.gameObject.layer == 8)
            {
                Zombie other = contact.collider.gameObject.GetComponent<Zombie>();
                if (other && Vector2.Dot(dir, other.dir) < -0.9)
                {
                    dir = -dir; // reverse move dir
                }
                else if (other && !other.justGotCarried && Mathf.Abs(contact.normal.x) > Mathf.Abs(contact.normal.y))
                {
                    other.justGotMounted = true;
                    justGotCarried = true;
                }
            }
            else if (Vector2.Dot(dir, contact.normal) < -0.9)
            {
                dir = -dir; // reverse move dir
                break;
            }
        }

        ContactFilter2D meatFilter = new ContactFilter2D();
        meatFilter.SetLayerMask(1 << 10);
        Collider2D[] meats = new Collider2D[8];
        sensor.OverlapCollider(meatFilter, meats);
        Collider2D closest = null;
        foreach (Collider2D meat in meats)
        {
            if (meat)
            {
                if (!closest)
                {
                    closest = meat;
                }
                else
                {
                    if (Vector3.Distance(transform.position, meat.transform.position) < Vector3.Distance(transform.position, closest.transform.position))
                    {
                        closest = meat;
                    }
                }
            }
        }
        if (closest)
        {
            dir.x = Mathf.Sign(closest.transform.position.x - transform.position.x);
        }
    }

    void OnDrawGizmos()
    {
        if (!Application.isPlaying) return;

        foreach (ContactPoint2D contact in contacts)
        {
            Gizmos.color = Color.red;
            Gizmos.DrawRay(contact.point, contact.normal * 0.1f);
        }
    }

    IEnumerator Growl()
    {
        yield return new WaitForSeconds(Random.Range(3f, 5f));
        audioSource.clip = growlClips[Random.Range(0, growlClips.Length - 1)];
        audioSource.Play();

        StartCoroutine(Growl());
    }
}
