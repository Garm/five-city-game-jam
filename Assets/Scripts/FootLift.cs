﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FootLift : MonoBehaviour
{
	public float interval = 0.5f;
	public float intervalOffset = 0.25f;
	public float force = 1.0f;

	float time = 0.0f;

	new Rigidbody2D rigidbody;

	void Start()
	{
		rigidbody = GetComponent<Rigidbody2D>();
	}

    void Update()
    {
		time += Time.deltaTime;

		if (time >= interval + intervalOffset)
		{
			rigidbody.AddForce(Vector2.up * force);
			time = 0;
		}
    }
}
