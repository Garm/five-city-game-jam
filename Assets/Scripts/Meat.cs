﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Meat : MonoBehaviour
{
    public float lifeTime = 5f;

    Vector3 startScale;
    public float eatenProgress = 0;

    public AudioClip hitClip;

    AudioSource audioSource;

    void Start()
    {
        audioSource = GetComponent<AudioSource>();

        startScale = transform.localScale;
    }

    void Update()
    {
        if (eatenProgress >= 1)
        {
            Destroy(gameObject);
        }
        else
        {
            transform.localScale = startScale * (1 - eatenProgress);
        }

		eatenProgress += Time.deltaTime * 0.2f;

        // ContactFilter2D zombieFilter = new ContactFilter2D();
        // zombieFilter.SetLayerMask(1 << 8);
        // Collider2D[] zombie = new Collider2D[1];
        // sensor.OverlapCollider(zombieFilter, zombie);
        // if (zombie[0])
        // {
        //     zombie[0].GetComponent<Zombie>().dir.x = Mathf.Sign(transform.position.x - zombie[0].transform.position.x);
        // }
    }

    void OnCollisionEnter2D(Collision2D collision)
    {
        if (collision.relativeVelocity.magnitude > 2f)
        {
            audioSource.PlayOneShot(hitClip);
        }
    }
}
