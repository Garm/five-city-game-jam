﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class GameManager : Singleton<GameManager>
{
	public static Zombie SpawnZombie(Vector3 position, int var)
	{
		Zombie prefab = Resources.Load<Zombie>("Prefabs/Zombie" + var.ToString());
		return Instantiate<Zombie>(prefab, position, Quaternion.identity);
	}

	public static Meat SpawnMeat(Vector3 position)
	{
		Meat prefab = Resources.Load<Meat>("Prefabs/Meat");
		return Instantiate<Meat>(prefab, position, Quaternion.identity);
	}
}
